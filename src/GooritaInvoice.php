<?php

namespace Goorita\Service;

use Carbon\Carbon;
use GuzzleHttp\Client;
use App\Entities\Balance;
use App\Entities\Invoice;
use Illuminate\Support\Facades\Auth;

class GooritaInvoice {
    public static function create($data)
    {
        $user = Auth::user();
        $data = array_merge([
            'expiry_date' => Carbon::now()->add(7, 'days')->format('Y-m-d H:i:sO'),
            'user_id' => $user->id,
            'status' => 0,
        ], $data);
        $inv = Invoice::create($data);
        return $inv;
    }

    public static function xendit($data)
    {
        $client = new Client();
        $response = $client->request('POST', env('URL_BALANCE').'create-invoice', [
            'json' => $data
        ]);
        $result = json_decode($response->getBody()->getContents());
        return $result;
    }
}